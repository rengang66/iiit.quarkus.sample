# quarkus.sample

#### 介绍
一些基于Quarkus的Java程序案例代码。详细讲解了在Quarkus平台上，如何进行Web、Data、Message、Security、Reactive、Tolerance、Health、Tracing、Spring集成等应用场景的开发和实现。

本源码是书籍《Quarkus实战，构建云原生java微服务》的应用案例源码。书籍《Quarkus实战，构建云原生java微服务》总共12章。其讲解顺序是：首先概述一下Quarkus的总体状况，让我们知道Quarkus的整体。其次是Quarkus的初探，这是让我们热身一下，暖暖场。再次就是本书的主要部分，详细讲解了在Quarkus架构上，如何进行Web、Data、Message、Security、Reactive、Tolerance、Health、Tracing、Spring集成等应用场景的开发和实现。再次说明Quarkus在云原生应用场景下实施和部署。最后是一个高级话题，就是的Quarkus的扩展。

#### 软件架构
软件架构说明

![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/213240_456c2ea1_1241472.jpeg "本书结构.jpeg")

#### 安装教程



#### 使用说明

第1章 Quarkus概述
    首先介绍Quarkus的概念及其特征。其次简单介绍Quarkus的优势。再次阐述了一下Quarkus的适用场景、目标用户和竞争对手。再次询问为什么java开发者会选择Quarkus。最后简介Quarkus框架的架构和核心概念。
第2章 Quarkus开发初探
    首先是开发一个Hello World微服务的全过程。其次说明Quarkus框架的开发基础，主要是六个基础应用的开发案例来讲解说明。再次用Quarkus实现Gof23设计模式的案例。最后说明应用案例的整体说明，也可以说是整个本书实战案例的导读。
第3章 开发REST/Web应用
    分别讲解了在Quarkus框架上如何开发Web（REST）、OpenAPI & SwaggerUI、GraphQL和WebSocket应用，包含案例的源码、讲解和验证。
第4章 数据持久化开发
    分别讲解了在Quarkus框架上如何开发Hibernate、JTA事务、Redis、MongoDB和Panache应用，包含案例的源码、讲解和验证。
第5章 整合消息流和消息中间件
    分别讲解了在Quarkus框架上如何开发Apache Kafka、JMS规范中Queue队列模式、JMS规范中Topic主题模式和MQTT协议应用，包含案例的源码、讲解和验证。
第6章 构建安全的Quarkus微服务
    介绍微服务Security概述和讲述Quarkus框架的Security架构。接着分别讲解了在Quarkus框架上如何开发用文件存储用户信息实现的安全认证应用，用数据库存储用户信息并采用JDBC获取的安全认证，用数据库存储用户信息并采用JPA获取的安全认证、采用Keycloak实现认证和授权的应用、使用OpenId Connect实现JAX-RS服务安全、使用OpenId Connect实现Web应用安全、使用JWT RBAC、使用OAuth2等的应用，包含案例的源码、讲解和验证。
第7章 构建响应式系统应用
    首先简介响应式系统的原因和基本概念。其次简介Quarkus框架的响应式应用。再次分别讲解了在Quarkus框架上如何开发响应式JAX-RS程序、响应式SQL Client程序、响应式Hibernate程序、响应式Redis程序、发响应式MongoDB程序、响应式Apache Kafka程序、响应式AMQP程序的应用，包含案例的源码、讲解和验证。最后讲述Quarkus框架的响应式基础框架Vert的程序应用，包含源码、讲解和验证。
第8章 Quarkus微服务容错机制
    首先介绍微服务容错的内容。接着讲述在Quarkus框架上如何开发包括重试、超时、回退、熔断器和隔离等微服务容错的应用。案例都包含案例的源码、讲解和验证。
第9章 Quarkus监控和日志
    首先介绍在Quarkus框架上如何开发健康监控应用，其次讲述在Quarkus框架上如何实现监控度量功能，最后讲述在Quarkus框架上如何开发调用链日志应用。这些应用都包含案例的源码、讲解和验证。
第10章 集成Spring框架到Quarkus中
    分别讲解了在Quarkus框架上如何整合Spring框架的DI功能、Spring框架的Web功能、Spring Data功能、Spring框架的安全功能、Springboot框架的属性文件功能、SpringCloud框架的ConfigServer配置文件功能的应用，包含案例的源码、讲解和验证。
第11章 Quarkus框架的云原生应用和部署
    分别讲解了在Quarkus框架上如何构建容器镜像的应用、如何生成Kubernetes资源文件的应用、如何生成OpenShift资源文件的应用、如何生成Knative资源文件的应用，包含案例的源码、讲解和验证。
第12章 高级应用——Quarkus框架的扩展
    首先概述Quarkus框架扩展的概念和内容。其次讲述在Quarkus框架上如何创建一个Quarkus框架扩展的应用，包含案例的讲解和验证。最后说明一些关于Quarkus框架扩展的内容。
后记
Quarkus也是不断发展过程中，本节主要讲述如何实现本文案例应用程序与Quarkus版本的与时俱进。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
